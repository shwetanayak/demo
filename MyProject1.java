package mypackage;

interface User{
	void msg();
}
class admin implements User{
	public void msg()
	{
		System.out.println("By admin hello world");
	}
}
class normuser extends admin
{
	public void msg1()
	{
		System.out.println("By norm user hello world");
	}
}

public class myclass {
	
public static void main(String args[]) {
	System.out.println("Main class Hello world");
	admin a=new admin();
	a.msg();
	normuser nu=new normuser();
	nu.msg1();
	
}
}
